# Nama Anggota
1. Fikri Ahmad (1606821955)
2. Nathaniel Nicholas (1606918370)
3. Raden Ajeng Wuriandita Wahyumurti Candra Kirana Dewi (1606917746)
4. Rafif Iqbal Shaputra (1606889465)

## Status & Report
[![pipeline status](https://gitlab.com/kelompok3a/Tugas2-PPW/badges/master/pipeline.svg)](https://gitlab.com/kelompok3a/Tugas2-PPW/commits/master)

[![coverage report](https://gitlab.com/kelompok3a/Tugas2-PPW/badges/master/coverage.svg)](https://gitlab.com/kelompok3a/Tugas2-PPW/commits/master)

## Link Heroku
http://unprepared.herokuapp.com