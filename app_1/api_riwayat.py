import requests

MATKUL_API	= 'https://private-e52a5-ppw2017.apiary-mock.com/riwayat'


def get_matkul(): # pragma: no cover
    matkul_list = requests.get(MATKUL_API)
    return matkul_list
