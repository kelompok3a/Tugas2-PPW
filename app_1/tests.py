
from django.test import TestCase

from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from django.http import HttpRequest
import unittest

class App1UnitTest(TestCase):

    def test_url_is_exist(self):
        response = Client().get('/app2-1/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/app2-1/')
        self.assertEqual(found.func, index)

