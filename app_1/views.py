from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from django.core import serializers

from .forms import Status_Form
from .models import Mahasiswa, Keahlian, User, Status
from .api_riwayat import get_matkul

response = {}
response["author"] = "Kelompok 3A"

# NOTE : untuk membantu dalam memahami tujuan dari suatu fungsi (def)
# Silahkan jelaskan menggunakan bahasa kalian masing-masing, di bagian atas
# sebelum fungsi tersebut.

# ======================================================================== #
# User Func
# Apa yang dilakukan fungsi INI? #silahkan ganti ini dengan penjelasan kalian
def index(request): # pragma: no cover
    # print ("#==> masuk index")
    if 'user_login' in request.session:
        return HttpResponseRedirect(reverse('app2-1:dashboard'))
    else:
        html = 'app_1/login.html'
        return render(request, html, response)

def dashboard(request): # pragma: no cover
    if 'user_login' in request.session:
        response['is_login'] = True
        
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username)
        my_Status= Status.objects.filter(user_id=response['user'].id)
        response['all_status'] = Status.objects.filter(user_id=response['user'].id)
        response['Status']= response['all_status'][::-1]

        if len(my_Status)== 0:
            response['last_status']='-'
        else:
            response['last_Status']=response['Status'][0]
            
        response['status_count'] = response['all_status'].count()
        response['status_form'] = Status_Form

        set_data_for_session(response,request)
        html = 'app_1/dashboard.html'
        return render(request, html, response)

    response['is_login'] = False
    html = 'app_1/login.html'
    return render(request, html, response)

def set_data_for_session(res, request): # pragma: no cover
    response['author'] = request.session['user_login']
    response['access_token'] = request.session['access_token']
    response['kode_identitas'] = request.session['kode_identitas']
    response['role'] = request.session['role']
    response['friends'] = Mahasiswa.objects.all()
    response['matkul_list'] = get_matkul().json()

	
def post_status(request): # pragma: no cover
    form = Status_Form(request.POST or None)
    if 'user_login' in request.session and request.method == 'POST' and form.is_valid():
        response['is_login'] = True
        
        username = request.session['user_login']
        kode_identitas = request.session['kode_identitas']
        status_message = request.POST['status']
        response['user'], is_login = User.objects.get_or_create(identity_number=kode_identitas, name=username)
        new_status = Status.objects.create(description=status_message, user=response['user'])
        return HttpResponseRedirect(reverse('app2-1:dashboard'))
        
    response['is_login'] = False
    html = 'app_1/login.html'
    return render(request, html, response)
