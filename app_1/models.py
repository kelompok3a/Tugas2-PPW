from django.db import models

class User(models.Model):
    identity_number = models.CharField(max_length=20)
    name = models.CharField(max_length=200)

class Status(models.Model):
    user = models.ForeignKey(User)
    description = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)

class Keahlian(models.Model):
    nama = models.CharField(max_length=27)
    tingkat = models.CharField(max_length=27)

    class Meta:
        ordering = ('nama',)
    
    def __str__(self): # pragma: no cover
        return self.nama + self.tingkat

class Mahasiswa(models.Model):
    npm = models.CharField(max_length=10)
    nama = models.CharField(max_length=27)
    angkatan = models.CharField(max_length=27)
    keahlian = models.ManyToManyField(Keahlian)

    class Meta:
        ordering = ('npm',)

    def __str__(self): # pragma: no cover
        return self.nama

